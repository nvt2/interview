# Novartis School of Excellence problem statement

## Explanation

You have been tasked with creating a program that assigns students to different classrooms based on their date of birth. The goal is to assign a group of students with the same birth year to each classroom. The data for the students and classrooms can be fetched from a fake API via the methods `getMockStudents() : Promise<Student[]>` and `getMockClassrooms : Promise<Classroom[]>`. For the input and desired output data you can orientate on the typing or sample data below.

```
getMockStudents(): Promise<Student[]>
/*
	Example:
	[
		{averageGrade: 78, dateOfBirth: "2001-01-15", internalPoints: 5, name: 'Eustaqui'},
		{averageGrade: 50, dateOfBirth: "2003-01-05", internalPoints: 0, name: 'Pau'},
		{averageGrade: 60, dateOfBirth: "2001-11-21", internalPoints: 1, name: 'Pepito'}
	]
*/
getMockClassrooms(): Promise<Classroom[]>
/*
	Example:
	[
		{year: 2001, name: 'flawless'},
		{year: 2003, name: 'marvelous'}
	]
*/
/* (map adding students array + sort by sum of averageGrade and internalPoints for all students on that classroom DESC)

	what gatherMergeAndSort method would return (example):
	[
		{
			students: [{averageGrade: 78, dateOfBirth: "2001-01-15", internalPoints: 5, name: 'Eustaqui'}, {averageGrade: 60, dateOfBirth: "2001-11-21", internalPoints: 1, name: 'Pepito'}]
			name: 'flawless',
			year: 2001
		},
		{
			students: [{averageGrade: 50, dateOfBirth: "2003-01-05", internalPoints: 0, name: 'Pau'}]
			name: 'marvelous',
			year: 2003
		},
	]
*/
```

## Unit testing
There is a file with some unit test suggestions, fill them and add new ones until you have 100% meaningful code coverage.

## Bonus
Finally, the classrooms array is sorted by the sum of all its students' grades and internal points in descendant order.

## Useful Scripts
package.json contains useful scripts you may want to use in order to develop faster this exercise: 
- test:watch will be running tests everytime index.ts or index.test.ts files ar echanged
- watch will run the function and console log the output with a fixed amount of 20 students
- start will execute the function with 1.000 students, better use it redirecting the output to a file!

Remember npm test will leave a file in /coverage/lcov-report/index.html with a very human friendly report
