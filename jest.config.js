module.exports = {
	roots: ["<rootDir>/src"],
	testPathIgnorePatterns: ["node_modules", "dist"],
	testMatch: ["**/*.test.ts"],
	transform: {
		"^.+\\.ts$": "ts-jest",
	},
	collectCoverage: true,
	coverageDirectory: "coverage",
};
