import { Student, Classroom } from "../types";

export const getMockStudents = async (): Promise<Student[]> => {
	const students: Student[] = [];
	const studentNames = [
		"Eustaqui",
		"Fernando",
		"Manuel",
		"Pau",
		"Pepito",
		"Surabhi",
	];
	for (let index = 0; index < 1000; index++) {
		students.push({
			dateOfBirth:
				"200" +
				(index % 10) +
				"-0" +
				((index % 9) + 1) +
				"-" +
				(index % 3) +
				"" +
				((index % 9) + 1),
			averageGrade: (1000 - index) % 100,
			internalPoints: (index * index) % 7,
			name: studentNames[index % 6],
		});
	}

	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(students);
		}, 2000);
	});
};

export const getMockClassrooms = async (): Promise<Classroom[]> => {
	const classrooms: Classroom[] = [];
	const classroomsNames = ["marvelous", "reallygood", "flawless"];
	for (let index = 0; index < 10; index++) {
		classrooms.push({
			year: 2000 + index,
			name: classroomsNames[index % 3],
		});
	}
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(classrooms);
		}, 2000);
	});
};


export const hasRecords = async (student: Student): Promise<boolean> => {
	return student.internalPoints > student.name.length;
};
