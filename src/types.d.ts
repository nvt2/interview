export type Classroom = {
	name: string;
	year: number;
};

export type Student = {
	averageGrade: number;
	dateOfBirth: string;
	internalPoints: number;
	name: string;
};
